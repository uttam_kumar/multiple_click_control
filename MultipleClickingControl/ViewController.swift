//
//  ViewController.swift
//  MultipleClickingControl
//
//  Created by Syncrhonous on 20/4/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    //var pressesInOneSecond = 0
    var timeStart: NSDate?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    var timer = Timer()
    var totalSecond = 10
    @IBAction func checkMultipleClick(_ sender: UIButton) {
        if totalSecond == 10 {
            startTimer()
        }
    }
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        //print(timeFormatted(totalSecond))
        print(totalSecond)
        if totalSecond != 0 {
            totalSecond -= 1
        } else {
            //end timer
            timer.invalidate()
            //reset
            totalSecond = 10
        }
    }
    
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        return String(format: "0:%02d", seconds)
    }

    
 
}

